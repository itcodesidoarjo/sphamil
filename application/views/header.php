<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SISTEM PAKAR | Gangguan Kehamilan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>asset/admin/img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/bootstrap.min.css">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/owl.transitions.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/normalize.css">
    <!-- meanmenu icon CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/meanmenu.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/main.css">
    <!-- morrisjs CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/vendor/modernizr-2.8.3.min.js"></script>
      <!-- summernote CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/summernote/summernote.css">

      <!-- touchspin CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/touchspin/jquery.bootstrap-touchspin.min.css">
    <!-- datapicker CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/datapicker/datepicker3.css">
    <!-- forms CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/form/themesaller-forms.css">
    <!-- colorpicker CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/colorpicker/colorpicker.css">
    <!-- select2 CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/select2/select2.min.css">
    <!-- chosen CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/chosen/bootstrap-chosen.css">
    <!-- ionRangeSlider CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/ionRangeSlider/ion.rangeSlider.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/ionRangeSlider/ion.rangeSlider.skinFlat.css">
     <!-- notifications CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/notifications/Lobibox.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/notifications/notifications.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/alerts.css">
    <!-- modals CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/admin/css/modals.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/admin/hhh/datatables/css/dataTables.bootstrap.min.css" />
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/hhh/custom/css/style.css" type="text/css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/css/data-table/bootstrap-editable.css">
  
</head>

<body>

    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="index.html"><img style="max-width:12mm;" class="main-logo" src="<?php echo base_url() ?>asset/user/img/banner/logo1.png" alt="" /></a>
                <strong><img src="<?php echo base_url() ?>asset/user/img/logo.png" alt="" /></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <?php if ($this->session->userdata('ses_type_user') == "1") { ?>
                    <ul class="metismenu" id="menu1">
                         <li><a title="Landing Page" href="<?php echo base_url('Home') ?>" aria-expanded="false"><i class="fa fa-institution icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Home</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('gejala') ?>" aria-expanded="false"><i class="fa fa-tint icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Gejala</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('Penyakit') ?>" aria-expanded="false"><i class="fa fa-heartbeat icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Penyakit</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('Pengetahuan') ?>" aria-expanded="false"><i class="fa fa-plus-square icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Pengetahuan</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('Riwayat') ?>" aria-expanded="false"><i class="fa fa-book icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Riwayat</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('logout') ?>" aria-expanded="false"><i class="fa fa-power-off icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Logout</span></a></li>
                    </ul>
                    <?php } ?>
                    <?php if ($this->session->userdata('ses_type_user') == "2") { ?>
                    <ul class="metismenu" id="menu1">
                         <li><a title="Landing Page" href="<?php echo base_url('Home') ?>" aria-expanded="false"><i class="fa fa-institution icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Home</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('Riwayat') ?>" aria-expanded="false"><i class="fa fa-book icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Riwayat</span></a></li>
                        <li><a title="Landing Page" href="<?php echo base_url('logout') ?>" aria-expanded="false"><i class="fa fa-power-off icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Logout</span></a></li>
                    </ul>
                    <?php } ?>
                </nav>
            </div>
        </nav>
    </div>
     <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                                    <i class="fa fa-bars"></i>
                                                </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                        
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                            <i class="fa fa-user adminpro-user-rounded header-riht-inf" aria-hidden="true"></i>
                                                            <span class="admin-name"><?=strtoupper($this->session->userdata('ses_username'));?></span>
                                                        </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <?php if ($this->session->userdata('ses_type_user') == "1") { ?>
                                    <ul class="mobile-menu-nav">
                                            <li><a title="Landing Page" href="<?php echo base_url('Home') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Home</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('gejala') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Gejala</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('Penyakit') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Penyakit</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('Pengetahuan') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Pengetahuan</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('Riwayat') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Riwayat</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('logout') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Logout</span></a></li>
                                    </ul>
                                    <?php } ?>
                                    <?php if ($this->session->userdata('ses_type_user') == "2") { ?>
                                    <ul class="mobile-menu-nav">
                                            <li><a title="Landing Page" href="<?php echo base_url('Home') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Home</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('Riwayat') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Data Riwayat</span></a></li>
                                            <li><a title="Landing Page" href="<?php echo base_url('logout') ?>" aria-expanded="false"><i class="fa fa-bookmark icon-wrap sub-icon-mg" aria-hidden="true"></i> <span class="mini-click-non">Logout</span></a></li>
                                    </ul>
                                    <?php } ?>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu end -->
            <br><br>
     