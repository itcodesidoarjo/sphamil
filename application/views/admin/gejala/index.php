<div class="breadcome-area">
        
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if($this->session->flashdata('pesan')): ?>
                    <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                <span class="icon-sc-cl" aria-hidden="true">×</span>
                            </button>
                        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                        <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                    </div>
                <?php endif; ?>
                 <?php if($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                <span class="icon-sc-cl" aria-hidden="true">×</span>
                            </button>
                        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                        <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                    </div>
                <?php endif; ?>
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Gejala <span class="table-project-n"></span> </h1>
                            <a href="<?php echo base_url('gejala/tambah');?>" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
                        </div>

                    </div>

                    <div class="sparkline13-graph">

                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>

                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                  <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>KODE</th>
                                        <th>GEJALA</th>
                                        <th>TANGGAL</th>
                                        
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no=1;
                                    foreach($gejala as $value) : 
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td>
                                            <a class="btn btn-custon-four btn-warning" href="<?php echo base_url('gejala/hapus/'.$value->id_gejala);?>" onclick="return confirm('Anda yakin akan menghapus?')"><i class="glyphicon glyphicon-trash"></i>
                                            </a> 
                                            <a class="btn btn-custon-four btn-info" href="<?php echo base_url('gejala/edit/'.$value->id_gejala);?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a> 
                                        </td>
                                        <td>
                                            <?= $value->kd_gejala; ?>
                                        </td>

                                        <td>
                                            <?= $value->nama_gejala; ?>
                                        </td>
                                        <td>
                                            <?= $value->tgl_gejala; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
