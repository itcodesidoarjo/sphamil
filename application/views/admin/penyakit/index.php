<div class="breadcome-area">
        
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Penyakit <span class="table-project-n"></span> </h1>
                            <a href="<?php echo base_url('penyakit/tambah');?>" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                  <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>KODE</th>
                                        <th>NAMA</th>
                                        <th>PENYEBAB</th>
                                        <th>SOLUSI</th>
                                        <th>TANGGAL</th>
                                        
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no=1;
                                    foreach($penyakit as $value) : 
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td>
                                            <a class="btn btn-custon-four btn-warning" href="<?php echo base_url('penyakit/hapus/'.$value->id_penyakit);?>" onclick="return confirm('Anda yakin akan menghapus?')"><i class="glyphicon glyphicon-trash"></i>
                                            </a> 
                                            <a class="btn btn-custon-four btn-info" href="<?php echo base_url('penyakit/edit/'.$value->id_penyakit);?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a> 
                                        </td>
                                        <td>
                                            <?= $value->kd_penyakit; ?>
                                        </td>

                                        <td>
                                            <?= $value->nama_penyakit; ?>
                                        </td>
                                        <td>
                                            <?= $value->penyebab_penyakit; ?>
                                        </td>
                                         <td>
                                            <?= $value->solusi_penyakit; ?>
                                        </td>
                                         <td>
                                            <?= $value->tgl_penyakit; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
