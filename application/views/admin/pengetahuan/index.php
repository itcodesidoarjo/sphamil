<div class="breadcome-area">
        
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Pengetahuan / Relasi <span class="table-project-n"></span> </h1>
                            <a href="<?php echo base_url('pengaduan/form_pengaduan');?>" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                  <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>nama</th>
                                        <th>Pengetahuan</th>
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no =1;
                                    $no1 =1;
                                    $no2 =1;
                                    $no3 =1;
                                    foreach($relasi as $value) : 
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td>
                                            <?php if (!empty($value->id_relasi)): ?>
                                            <a class="btn btn-custon-four btn-info" href="<?php echo base_url('pengetahuan/edit/'.$value->id_penyakit);?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a>
                                            <?php endif ?>
                                            <?php if (empty($value->id_relasi)): ?>
                                            <a class="btn btn-custon-four btn-success" href="<?php echo base_url('pengetahuan/tambah/'.$value->id_penyakit);?>"><i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                            <?php endif ?>
                                        </td>
                                        <td><?=$value->nama_penyakit?></td>
                                        <td>
                                            <div class="admin-pro-accordion-wrap responsive-mg-b-30">
                                                <div class="panel-group adminpro-custon-design" id="accordion">
                                                    <div class="panel panel-danger">
                                                        <div class="panel-heading accordion-head">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$no1++?>" aria-expanded="false" class="collapsed">
                                                         Gejala <?=$value->nama_penyakit?></a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse<?=$no3++?>" class="panel-collapse panel-ic collapse" aria-expanded="false" style="height: 0px;">
                                                            <div class="panel-body admin-panel-content animated bounce">
                                                                <?php  foreach($diagnosa as $key) : 
                                                                    if ($key->id_penyakit == $value->id_penyakit && $key->id_relasi == $value->id_relasi ) {
                                                                ?>
                                                                     <ul> 
                                                                          <li><?=$key->kd_gejala?> - <?=$key->nama_gejala?> <b>(<?=$key->pilih_relasi?>)</b>
                                                                          </li>
                                                                      </ul>
                                                                <?php }
                                                                endforeach; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
