<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <?php if($this->session->flashdata('pesan')): ?>
            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-plus" aria-hidden="true"></i>Tambah Data Pengetahuan</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <?php echo form_open_multipart('pengetahuan/input_data'); ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" width="100%">
                          <thead>
                            <tr>
                                <?php foreach ($penyakit as $value) { ?>
                                <th colspan="14">
                                        PENYAKIT : <?='['.$value->kd_penyakit.'] '.$value->nama_penyakit  ?>               
                                </th>
                               
                            </tr>
                            <tr style="background:#d22004;color:#fff;">
                              <th colspan="14" style="text-align:center">GEJALA</th>
                            </tr>
                            <tr style="background:#d22004;color:#fff;">
                              <th>KODE</th>
                              <th>NAMA</th>
                              <th colspan="13">PILIHAN</th>
                            </tr>
                          </thead>
                          <tbody>
                                <?php foreach ($gejala as $val) { ?>
                                <tr>
                                  <th style="display:none"><input type="text" name="id_gejala[]" value="<?=$val->id_gejala?>"></th>
                                  <th style="display:none"><input type="text" name="id_penyakit" value="<?=$value->id_penyakit?>"></th>
                                  <th width="1%"><?=$val->kd_gejala?></th>
                                  <td width="94%"><?=$val->nama_gejala?></td>
                                  <td width="5%">
                                    <select class="form-control" name="pilih_relasi[]" style="width:80px;">
                                      <option value="Tidak">Tidak</option>
                                      <option value="Ya" selected="">Ya</option>
                                    </select>
                                  </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                      </div>
                       <?php } ?>
                  <br>
                    <center>
                    <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                    </button>
                    <a href="<?php echo base_url() ?>pengetahuan" class="btn btn-warning">Batal
                    </a>
                    </center>

                <?php echo form_close(); ?>
            </div>
        </div>
</div>
