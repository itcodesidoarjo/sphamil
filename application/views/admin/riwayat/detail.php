<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SISTEM PAKAR | Gangguan Kehamilan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>asset/admin/img/favicon.ico">
    <!-- Google Fonts
    ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/bootstrap.min.css">
    <!-- Bootstrap CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/font-awesome.min.css">
    <!-- owl.carousel CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/owl.transitions.css">
    <!-- animate CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/animate.css">
    <!-- normalize CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/normalize.css">
    <!-- main CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/main.css">
    <!-- morrisjs CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/calendar/fullcalendar.print.min.css">
    <!-- forms CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/form/all-type-forms.css">
    <!-- style CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/style.css">
    <!-- responsive CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/css/responsive.css">
    <!-- modernizr JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="color-line"></div>
    
    <div class="panel-body">

  <fieldset class="content-group">
  <div class="alert alert-info alert-dismissible" role="alert">
    <center>
     <strong>HASIL DIAGNOSA MASALAH KEHAMILAN METODE FORWARD CHAINING</strong>
   </center>
  </div>
  <br>

  <style>
    #jarak{
        padding-left:0px;padding-right:0px;vertical-align:top;
    }
    #v_top{
      vertical-align:top;
    }
  </style>


  <table class="table" width="100%">
      <tbody>
        <?php foreach ($user as $value) { ?>
          <tr>
            <th width="16%" id="v_top">NAMA LENGKAP</th>
            <th width="1%" id="jarak">:</th>
            <td width="87%" id="jarak"><?=$value->nama_user?></td>
          </tr>
          <tr>
            <th id="v_top">E-MAIL</th>
            <th id="jarak">:</th>
            <td id="jarak"><?=$value->email_user?></td>
          </tr>
          <tr>
            <th id="v_top">NO. HP</th>
            <th id="jarak">:</th>
            <td id="jarak"><?=$value->telepon_user?></td>
          </tr>
          <tr>
            <th id="v_top">ALAMAT</th>
            <th id="jarak">:</th>
            <td id="jarak"><?=$value->alamat_user?></td>
          </tr>
        <?php } ?>
          <tr>
            <th id="v_top">JAWABAN PENGGUNA</th>
            <th id="jarak">:</th>
            <td id="jarak">
              <ul> 
                  <?php foreach ($diagnosa as $value1) { ?>
                  <li><?=$value1->kd_gejala?> - <?=$value1->nama_gejala?> <b>(<?=$value1->jawab_diagnosa?>)</b></li>
                  <?php } ?>
              </ul>
            </td>
          </tr>
          <tr>
            <th id="v_top">HASIL <br> <div style="font-size:10px;">Forward Chaining</div> </th>
            <th id="jarak">:</th>
            <td id="jarak">
              
             <?php if (!empty($penyakit['id_relasi'])) {?>
              <p style="white-space: pre-wrap;">Berdasarkan gejala yang <b><?=$penyakit['username']?></b> alami, <?=$penyakit['nama_penyakit']?> <b>Masalah Kehamilan</b>.</p> 
              <?php }else{ ?> 
              <p style="white-space: pre-wrap;">Berdasarkan gejala yang <b><?=$penyakit['username']?></b> alami, kami tidak menemukan <b>Masalah Kehamilan</b>.</p> 
              <?php } ?>   
            </td>  
          </tr>
          <tr>
            <th id="v_top">PENYEBAB</th>
            <th id="jarak">:</th>
            <td id="jarak">
              <p style="white-space: pre-wrap;"><?=$penyakit['penyebab_penyakit']?></p>    </td>
          </tr>
          <tr>
            <th id="v_top">SOLUSI</th>
            <th id="jarak">:</th>
            <td id="jarak">
              <p style="white-space: pre-wrap;"><?=$penyakit['solusi_penyakit']?></p>    </td>
          </tr>
          <tr>
            <td colspan="3"><center><a href="<?php echo base_url('riwayat/pdf')?>" target="_blank" class="btn btn-success"><i class="icon-printer2"></i> CETAK HASIL DIAGNOSA</a></center></td>
          </tr>
          
    </tbody>
  </table>
  </fieldset><div class="container-fluid">
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <center>
                  <a href="<?php echo base_url('#home')?>" class="btn btn-danger">Back to Dashboard</a> &nbsp;
                  <a href="<?php echo base_url('logout/diagnosa')?>" class="btn btn-primary"><i class="icon-search4"></i> Kembali Melakukan Diagnosa</a>
              </center>
          </div>
      </div>
  </div>
</div>


    <!-- jquery
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/bootstrap.min.js"></script>
    <!-- wow JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/wow.min.js"></script>
    <!-- price-slider JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/owl.carousel.min.js"></script>
    <!-- sticky JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo base_url()?>asset/admin/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url()?>asset/admin/js/metisMenu/metisMenu-active.js"></script>
    <!-- tab JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/tab.js"></script>
    <!-- icheck JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/icheck/icheck.min.js"></script>
    <script src="<?php echo base_url()?>asset/admin/js/icheck/icheck-active.js"></script>
    <!-- plugins JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/plugins.js"></script>
    <!-- main JS
    ============================================ -->
    <script src="<?php echo base_url()?>asset/admin/js/main.js"></script>
</body>

</html>