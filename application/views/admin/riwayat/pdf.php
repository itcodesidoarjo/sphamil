
<!DOCTYPE html>
<html>
    <head>
        <style>
            body{
                font-family : Arial;
                font-style: bold;
                margin: 0;
                background-color: #404040;
            }
            .page {
                width: 210mm;
                min-height: 297mm;
                padding: 1mm;
                margin: 0mm auto;
                background: white;
            }
            .subpage {
                margin-left: 14mm;
                margin-right: 4mm;
                margin-top: 4mm;
            }

            @page {
                size: A4 portrait;
                margin: 0;
            }

            @media print {
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
             #jarak{
                padding-left:0px;padding-right:0px;vertical-align:top;
            }
            #v_top{
              vertical-align:top;
            }
        </style>

    </head>




<body>
    <div class="page">
        <div class="subpage">
           
        <hr style="margin-top:2px;">
        <h5 align="center" style="margin-top: 0px;"><strong>HASIL DIAGNOSA MASALAH KEHAMILAN METODE FORWARD CHAINING</strong>
        </h5>
        
        

        <hr style="margin-top:0px; margin-bottom:5px; margin-top: -15px; ">
        <table border="0" class="table" width="100%">
            <tbody>
                <?php foreach ($user as $value) { ?>
                  <tr valign="top" align="left">
                    <td width="16%" >NAMA LENGKAP</td>
                    <td width="1%" >:</td>
                    <td valign="top" align="left" width="87%" ><?=$value->nama_user?></td>
                  </tr>
                  <tr valign="top" align="left">
                    <td >E-MAIL</td>
                    <td >:</td>
                    <td valign="top" align="left" ><?=$value->email_user?></td>
                  </tr>
                  <tr valign="top" align="left">
                    <td >NO. HP</td>
                    <td >:</td>
                    <td valign="top" align="left" ><?=$value->telepon_user?></td>
                  </tr>
                  <tr valign="top" align="left">
                    <td >ALAMAT</td>
                    <td >:</td>
                    <td valign="top" align="left" ><?=$value->alamat_user?></td>
                  </tr>
                <?php } ?>
                  <tr valign="top" align="left">
                    <td >JAWABAN PENGGUNA</td>
                    <td >:</td>
                    <td valign="top" align="left" >
                      <ul valign="top"> 
                          <?php foreach ($diagnosa as $value1) { ?>
                          <li ><?=$value1->kd_gejala?> - <?=$value1->nama_gejala?> <b>(<?=$value1->jawab_diagnosa?>)</b></li>
                          <?php } ?>
                      </ul>
                    </td>
                  </tr>
                  <tr valign="top" align="left">
                    <td >HASIL <br> <div style="font-size:10px;">Forward Chaining</div> </td>
                    <td >:</td>
                    <td valign="top" align="left" >
                      
                     <?php if (!empty($penyakit['id_relasi'])) {?>
                      Berdasarkan gejala yang <b><?=$penyakit['username']?></b> alami, <?=$penyakit['nama_penyakit']?> <b>Masalah Kehamilan</b>.
                      <?php }else{ ?> 
                      Berdasarkan gejala yang <b><?=$penyakit['username']?></b> alami, kami tidak menemukan <b>Masalah Kehamilan</b>.
                      <?php } ?>   
                    </td>  
                  </tr>
                  <tr valign="top" align="left">
                    <td >PENYEBAB</td>
                    <td >:</td>
                    <td valign="top" align="left" >
                      <?=$penyakit['penyebab_penyakit']?>   </td>
                  </tr>
                  <tr valign="top" align="left">
                    <td >SOLUSI</td>
                    <td >:</td>
                    <td valign="top" align="left" >
                      <?=$penyakit['solusi_penyakit']?>   </td>
                  </tr>
                  <tr valign="top" align="left">
                    
                  </tr>
                  
            </tbody>
          
        </table>       
    </div>
</div>
</body>
</html>
