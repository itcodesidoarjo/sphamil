<div class="breadcome-area">
        
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Riwayat Diagnosa <span class="table-project-n"></span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                  <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>No. Tlpn</th>
                                        <th>Alamat</th>
                                        <th>Tanggal</th>
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no=1;
                                    foreach($riwayat as $value) : 
                                    ?>
                                    <tr><td><?=$no++?></td>
                                        <td>
                                           <a class="btn btn-custon-four btn-warning" href="<?php echo base_url('riwayat/detail/'.$value->id_user);?>" target="_blank" ><i class="glyphicon glyphicon-list"></i> 
                                            </a> 
                                            <a class="btn btn-custon-four btn-info" target="_blank"  href="<?php echo base_url('riwayat/pdf/'.$value->id_user);?>"><i class="fa fa-print" aria-hidden="true"></i>
                                            </a> 
                                        </td>
                                        
                                        <td>
                                            <?= $value->nama_user; ?>
                                        </td>

                                        <td>
                                            <?= $value->email_user; ?>
                                        </td>
                                        <td>
                                            <?= $value->telepon_user; ?>
                                        </td>
                                        <td>
                                            <?= $value->alamat_user; ?>
                                        </td>
                                        <td>
                                            <?= $value->tglreg_user; ?>
                                        </td>
                                        
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
