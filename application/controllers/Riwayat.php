<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }

	public function index()
	{
		$id 				= $this->session->userdata('ses_id_user');
		$type 				= $this->session->userdata('ses_type_user');
		$data['riwayat'] 	= $this->mriwayat->index_riwayat($id,$type);

	    $this->load->view('header.php');
		$this->load->view('admin/riwayat/index.php',$data);
		$this->load->view('footer.php');
	}

	public function detail()
	{
		$id 				= $this->session->userdata('ses_id_user');

		$data['user'] 		= $this->mriwayat->index_user($id);
		$data['diagnosa'] 	= $this->mriwayat->index_diagnosa($id);
		$data['penyakit'] 	= $this->mriwayat->index_penyakit($id);

		$this->load->view('admin/riwayat/detail.php',$data);
	}

	public function pdf()
	{
		$id 				= $this->session->userdata('ses_id_user');

		$data['user'] 		= $this->mriwayat->index_user($id);
		$data['diagnosa'] 	= $this->mriwayat->index_diagnosa($id);
		$data['penyakit'] 	= $this->mriwayat->index_penyakit($id);

		$this->load->view('admin/riwayat/pdf.php',$data);
	}

	public function detail_index($id)
	{

		$data['user'] 		= $this->mriwayat->index_user($id);
		$data['diagnosa'] 	= $this->mriwayat->index_diagnosa($id);
		$data['penyakit'] 	= $this->mriwayat->index_penyakit($id);

		$this->load->view('admin/riwayat/detail.php',$data);
	}

	public function pdf_index($id)
	{

		$data['user'] 		= $this->mriwayat->index_user($id);
		$data['diagnosa'] 	= $this->mriwayat->index_diagnosa($id);
		$data['penyakit'] 	= $this->mriwayat->index_penyakit($id);

		$this->load->view('admin/riwayat/pdf.php',$data);
	}

}
