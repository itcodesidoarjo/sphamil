<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gejala extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
    
	public function index()
	{
		$data['gejala'] = $this->mgejala->index_gejala();
	    $this->load->view('header.php');
		$this->load->view('admin/gejala/index.php',$data);
		$this->load->view('footer.php');
	}

	public function tambah()
	{
		// $data['gejala'] = $this->mgejala->index_gejala();
	    $this->load->view('header.php');
		$this->load->view('admin/gejala/tambah.php');
		$this->load->view('footer.php');
	}

	public function input_data()
	{
		$kode 		= $_POST['kd_gejala'];
		$cek_kode 	= $this->mgejala->cekkode($kode);

		if (empty($cek_kode)) {
			date_default_timezone_set('Asia/Jakarta');
	     	$jam 		= date("Y-m-d h:i:s");
	     	
		      $data = [
		       'kd_gejala' 			=> set_value('kd_gejala'),
		       'nama_gejala' 		=> set_value('nama_gejala'),
		       'tgl_gejala' 		=> $jam,
		     ];
	     	
		      $this->mgejala->simpan($data); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('gejala');
		}else{
			$this->session->set_flashdata('error','Gagal disimpan, kode gejala sudah digunakan.');
		    redirect('gejala/tambah');

		}
	}

	public function hapus($id)
	{
		$data = [
           'status_gejala' 		=> 2,
     	];
      	$where = array(
			'id_gejala' => $id
		);

		$hapus = $this->mgejala->hapus($data,$where);
		$this->session->set_flashdata('pesan','Data Gejala berhasil di hapus');
		redirect('gejala');
		
	}

	public function edit($id)
	{
		$data['gejala'] = $this->mgejala->edit($id);
	    $this->load->view('header.php');
		$this->load->view('admin/gejala/edit.php',$data);
		$this->load->view('footer.php');
	}

	public function simpan_edit()
	{
		$kode 		= $_POST['kd_gejala'];
		$id 		= $_POST['id_gejala'];
		$cek_kode 	= $this->mgejala->cekkodeedit($kode,$id);

		if (empty($cek_kode)) {
			date_default_timezone_set('Asia/Jakarta');
	     	$jam 		= date("Y-m-d h:i:s");
	     	
		    $data = [
		       'kd_gejala' 			=> set_value('kd_gejala'),
		       'nama_gejala' 		=> set_value('nama_gejala'),
		    ];

		    $where = array(
				'id_gejala' => $id
			);
	     	
		      $this->mgejala->simpanedit($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('gejala');
		}else{
			$this->session->set_flashdata('error','Gagal disimpan, kode gejala sudah digunakan.');
		    redirect('gejala/edit/'.$id);

		}
	}
}
