<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/registrasi.php');
	}

	public function loginauth()
  	{
     
          $email_user  =htmlspecialchars($this->input->post('email_user',TRUE),ENT_QUOTES);
          $password     =htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

          $cek_login  =$this->mlogin->auth_login_user($email_user,$password);

          if($cek_login->num_rows() > 0){ //jika login sebagai dosen
                $data=$cek_login->row_array();
                      $data_session = array(
                     'status'   => "login",
                     'akses'    => "2",
                     'user_in'  => true
                       );
                      $this->session->set_userdata('akses','2');
                      $this->session->set_userdata('ses_id_user',$data['id_user']);
                      $this->session->set_userdata('ses_nama_user',$data['nama_user']);
                      $this->session->set_userdata('ses_telepon_user',$data['telepon_user']);
                      $this->session->set_userdata('ses_username',$data['username']);
                      $this->session->set_userdata('ses_email_user',$data['email_user']);
                      $this->session->set_userdata('ses_alamat_user',$data['alamat_user']);
                      $this->session->set_userdata('ses_tglreg_user',$data['tglreg_user']);
                      $this->session->set_userdata('ses_type_user',$data['type_user']);
                      $this->session->set_userdata($data_session);

                      /*behasil*/
                      $this->session->set_flashdata('pesan', 'Berhasil Login.');
                      if ($data['type_user']=="1") {
                        redirect(base_url("home"));
                      }else{
                        redirect(base_url("diagnosa"));
                      }
                   
            }else{
                 $this->session->set_flashdata('error', 'Email dan password yang anda masukkan tidak ditemukan.');
                 redirect(base_url("#clockdiv"));
            }
  }


}
