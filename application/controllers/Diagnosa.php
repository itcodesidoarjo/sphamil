<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	// public function __construct(){
 //        parent::__construct();
 //        if($this->session->userdata('status') != "login"){
 //            redirect(base_url("welcome"));
 //        }
 //    }
    
	public function index()
	{
		$data 			  = $this->mdiagnosa->index_diagnosa($this->session->userdata('ses_id_user'));
		$id_gejala 		  = empty($data['gejala'])?'':$data['gejala'];
		$data['diagnosa'] = $this->mdiagnosa->index_gejala($id_gejala);

		if (!empty($data['diagnosa'])) {
			$this->load->view('admin/diagnosa.php',$data);
		}else{
			$data_jawab 	= $this->mdiagnosa->index_jawaban($this->session->userdata('ses_id_user'));
			$data_relasi 	= $this->mdiagnosa->index_relasi();
			
			$copy1 =[];
			foreach ($data_relasi as $key => $value) {
				if ($data_jawab['gejala'] == $value['gejala'] && $data_jawab['jawab'] == $value['pilih'] ) {

					$copy['id_relasi'] 				= $value['id_relasi'];
					$copy['tgl_hasil_diagnosa'] 	= date("Y-m-d h:i:s");
					$copy['id_user'] 				= $this->session->userdata('ses_id_user');
					$copy1[] = $copy; 
				}
			}

			if (empty($copy1)) {
				 $copy1 = [
			       'id_user' 			=> $this->session->userdata('ses_id_user'),
			       'id_relasi' 			=> "0",
			       'tgl_hasil_diagnosa' => date("Y-m-d h:i:s"),
			     ];

			    $id = $this->mdiagnosa->simpan_hasil($copy1);
			}else{
			    $id = $this->mdiagnosa->simpan_hasil2($copy1);
			}
			$id = $this->mdiagnosa->data_hasil($this->session->userdata('ses_id_user'));

	     	redirect('riwayat/detail/'.$id['id_hasildiagnosa']);
		}
	}

	public function input_jawaban()
	{
		date_default_timezone_set('Asia/Jakarta');
     	$jam 		= date("Y-m-d h:i:s");
     	
	      $data = [
	       'id_user' 			=> $this->session->userdata('ses_id_user'),
	       'id_gejala' 			=> set_value('id_gejala'),
	       'jawab_diagnosa' 	=> set_value('jawab_diagnosa'),
	       'tgl_diagnosa' 		=> $jam,
	     ];
     	
	      $this->mdiagnosa->simpan($data); 
          $this->session->set_flashdata('pesan','Berhasil disimpan.');
	      redirect('diagnosa');
		// }else{
		// 	$this->session->set_flashdata('error','Gagal disimpan, kode penyakit sudah digunakan.');
		//     redirect('penyakit/tambah');

		// }
	}
}
