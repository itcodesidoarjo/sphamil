<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengetahuan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
    
	public function index()
	{
		$data['diagnosa'] = $this->mrelasi->dt_relasi_diagnosa();
		$data['relasi'] = $this->mrelasi->index_relasi();
	    $this->load->view('header.php');
		$this->load->view('admin/pengetahuan/index.php',$data);
		$this->load->view('footer.php');
	}

	public function tambah($id)
	{
		$data['penyakit'] = $this->mrelasi->dt_penyakit($id);
		$data['gejala'] = $this->mrelasi->dt_gejala();
	    $this->load->view('header.php');
		$this->load->view('admin/pengetahuan/tambah.php',$data);
		$this->load->view('footer.php');
	}

	public function input_data()
	{
			$gejala =$_POST['id_gejala'];
			$relasi =$_POST['pilih_relasi'];
			$copy1 =[];
			$no = 0;

		    $data = [
		       'id_penyakit' 		=> set_value('id_penyakit'),
		    ];

		    $id = $this->mrelasi->simpan($data); 
		    
		    foreach ($gejala as $key => $value1) {
					$copy['pilih_relasi'] = $relasi[$no++];
					$copy['id_gejala'] = $value1;
					$copy['id_relasi'] = $id;
					$copy1[] = $copy; 
			}

		   	$idd = $this->mrelasi->simpan_detail($copy1); 
		   
	        $this->session->set_flashdata('pesan','Berhasil disimpan.');
		    redirect('pengetahuan');
	}
	public function edit($id)
	{
		$data['penyakit'] = $this->mrelasi->dt_penyakitedit($id);
		// $data['gejala'] = $this->mrelasi->dt_gejala();
		$data['gejala'] = $this->mrelasi->index_relasiedit($id);
	    $this->load->view('header.php');
		$this->load->view('admin/pengetahuan/edit.php',$data);
		$this->load->view('footer.php');
	}

	public function edit_data()
	{
			$gejala =$_POST['id_gejala'];
			$relasi =$_POST['pilih_relasi'];
			$id 	=$_POST['id_relasi'];
			$copy1 	=[];
			$no 	= 0;

		   	$delete = $this->mrelasi->delete_edit($id); 

		    foreach ($gejala as $key => $value1) {
					$copy['pilih_relasi'] = $relasi[$no++];
					$copy['id_gejala'] = $value1;
					$copy['id_relasi'] = $id;
					$copy1[] = $copy; 
			}

		   	$idd = $this->mrelasi->simpan_detail($copy1); 
		   
	        $this->session->set_flashdata('pesan','Berhasil disimpan.');
		    redirect('pengetahuan');
	}

}
