<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/registrasi.php');
	}

	public function tambah()
	{
		$data['title']='Registrasi';

		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
		$this->form_validation->set_rules('email_user', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');
		if($this->form_validation->run() === FALSE){
				$this->session->set_flashdata('error','Gagal disimpan.');
				$this->load->view('admin/registrasi');
			} else {

				// Encrypt password
				$enc_password = md5($this->input->post('password'));

				$this->mregister->register($enc_password);

				// Set set_message
				$this->session->set_flashdata('user_masuk', 'You are now registered and can log in');
				redirect(base_url("login"));
			}
	}

	// Check if username exists
	public function check_username_exists($username){
		$this->form_validation->set_message('check_username_exists', 'Userneme yang anda masukkan telah digunakan!');
		if($this->mregister->check_username_exists($username)){
			return true;
		} else {
			return false;
		}
	}

		// Check if email exists
	public function check_email_exists($email_user){
		$this->form_validation->set_message('check_email_exists', 'Email yang anda masukkan telah digunakan!');
		if($this->mregister->check_email_exists($email_user)){
			return true;
		} else {
			return false;
		}
	}

}
