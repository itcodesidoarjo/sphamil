 <?php
class Logout extends CI_Controller{

  public function index(){
  		if($this->session->userdata('logged_in')){
	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('admin'));
	     }

	     if($this->session->userdata('user_in')){
	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('#home'));
	     }
	
    }

     public function diagnosa(){
  		if($this->session->userdata('logged_in')){
	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('register'));
	     }

	     if($this->session->userdata('user_in')){
	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('register'));
	     }
	
    }
}
?>