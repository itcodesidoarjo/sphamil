<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyakit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
    
	public function index()
	{
		$data['penyakit'] = $this->mpenyakit->index_penyakit();
	    $this->load->view('header.php');
		$this->load->view('admin/penyakit/index.php',$data);
		$this->load->view('footer.php');
	}

	public function tambah()
	{
	    $this->load->view('header.php');
		$this->load->view('admin/penyakit/tambah.php');
		$this->load->view('footer.php');
	}

	public function input_data()
	{
		$kode 		= $_POST['kd_penyakit'];
		$cek_kode 	= $this->mpenyakit->cekkode($kode);

		if (empty($cek_kode)) {
			date_default_timezone_set('Asia/Jakarta');
	     	$jam 		= date("Y-m-d h:i:s");
	     	
		      $data = [
		       'kd_penyakit' 		=> set_value('kd_penyakit'),
		       'nama_penyakit' 		=> set_value('nama_penyakit'),
		       'solusi_penyakit' 	=> set_value('solusi_penyakit'),
		       'penyebab_penyakit' 	=> set_value('penyebab_penyakit'),
		       'tgl_penyakit' 		=> $jam,
		     ];
	     	
		      $this->mpenyakit->simpan($data); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('penyakit');
		}else{
			$this->session->set_flashdata('error','Gagal disimpan, kode penyakit sudah digunakan.');
		    redirect('penyakit/tambah');

		}
	}

	public function hapus($id)
	{
		$data = [
           'status_penyakit' 		=> 2,
     	];
      	$where = array(
			'id_penyakit' => $id
		);

		$hapus = $this->mpenyakit->hapus($data,$where);
		$this->session->set_flashdata('pesan','Data penyakit berhasil di hapus');
		redirect('penyakit');
		
	}

	public function edit($id)
	{
		$data['penyakit'] = $this->mpenyakit->edit($id);
	    $this->load->view('header.php');
		$this->load->view('admin/penyakit/edit.php',$data);
		$this->load->view('footer.php');
	}

	public function simpan_edit()
	{
		$kode 		= $_POST['kd_penyakit'];
		$id 		= $_POST['id_penyakit'];
		$cek_kode 	= $this->mpenyakit->cekkodeedit($kode,$id);

		if (empty($cek_kode)) {
			date_default_timezone_set('Asia/Jakarta');
	     	$jam 		= date("Y-m-d h:i:s");
	     	
		    $data = [
		       'kd_penyakit' 		=> set_value('kd_penyakit'),
		       'nama_penyakit' 		=> set_value('nama_penyakit'),
		       'nama_penyakit' 		=> set_value('nama_penyakit'),
		       'solusi_penyakit' 	=> set_value('solusi_penyakit'),
		       'penyebab_penyakit' 	=> set_value('penyebab_penyakit'),
		    ];

		    $where = array(
				'id_penyakit' => $id
			);
	     	
		      $this->mpenyakit->simpanedit($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('penyakit');
		}else{
			$this->session->set_flashdata('error','Gagal disimpan, kode penyakit sudah digunakan.');
		    redirect('penyakit/edit/'.$id);

		}
	}
}
