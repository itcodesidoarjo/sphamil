<?php
	class Mpenyakit extends CI_Model
{
	public function index_penyakit(){
    	$query=$this->db->query("
    		SELECT * FROM tbl_penyakit where status_penyakit ='1'"
    	);
   	 return $query->result();
	}

	public function cekkode($kode){
    	$query=$this->db->query("
    	SELECT * FROM tbl_penyakit where kd_penyakit = '".$kode."'"
    	);
   	 return $query->result();
	}

	public function simpan($data)
	{
	   $this->db->insert('tbl_penyakit', $data);
	   $id = $this->db->insert_id();
	}

	public function hapus($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_penyakit',$data);
	}

	public function edit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_penyakit where true and status_penyakit ='1' and id_penyakit = ".$id
    	);
   	 return $query->result();
	}

	public function cekkodeedit($kode,$id){
    	$query=$this->db->query("
    	SELECT * FROM tbl_penyakit where true and kd_penyakit = '".$kode."' and id_penyakit <> '".$id."'"
    	);
   	 return $query->result();
	}

	public function simpanedit($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_penyakit',$data);
	}
    	
}