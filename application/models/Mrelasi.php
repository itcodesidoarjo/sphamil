<?php
	class Mrelasi extends CI_Model
{
	public function index_relasi(){
    	$query=$this->db->query("
    		SELECT a.id_penyakit, nama_penyakit, kd_penyakit, b.id_relasi FROM tbl_penyakit a
			LEFT JOIN tbl_relasi b ON a.id_penyakit = b.id_penyakit
			WHERE status_penyakit ='1' "
    	);
   	 return $query->result();
	}

	public function dt_penyakit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_penyakit where status_penyakit ='1' and id_penyakit = ".$id
    	);
   	 return $query->result();
	}

	public function dt_gejala(){
    	$query=$this->db->query("
    		SELECT * FROM tbl_gejala where status_gejala ='1'"
    	);
   	 return $query->result();
	}

	public function simpan($data)
	{
	   $this->db->insert('tbl_relasi', $data);
	   $id = $this->db->insert_id();

	   return $id;
	}

	public function simpan_detail($data)
	{
	   foreach ($data as $key => $value) {
	   	  	$query=$this->db->query("
	    		SELECT * FROM tbl_relasi_detail where TRUE AND id_relasi = '".$value['id_relasi']."' AND id_gejala = '".$value['id_gejala']."' "
	    	);
	    	if (empty($query->result())) {
	    		 $this->db->insert('tbl_relasi_detail', $value);
	   		 	 $id = $this->db->insert_id();
	    	}
	   }
	}
    
    public function index_relasiedit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_relasi_detail a
			LEFT JOIN tbl_relasi b ON a.id_relasi = b.id_relasi
			LEFT JOIN tbl_penyakit c ON c.id_penyakit = c.id_penyakit  
			LEFT JOIN tbl_gejala d ON d.id_gejala = a.id_gejala 
			WHERE status_penyakit ='1' AND c.id_penyakit = '".$id."'  AND b.id_penyakit = '".$id."' "
    	);
   	 return $query->result();
	}

	public function dt_penyakitedit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_penyakit a
			LEFT JOIN tbl_relasi b ON a.id_penyakit = b.id_penyakit WHERE status_penyakit ='1' AND a.id_penyakit = ".$id
    	);
   	 return $query->result();
	}

	public function delete_edit($id){
    	$query=$this->db->query("
    		DELETE FROM tbl_relasi_detail WHERE id_relasi = ".$id
    	);
	}

	public function dt_relasi_diagnosa(){
    	$query=$this->db->query("
    		SELECT * FROM tbl_relasi_detail a
			LEFT JOIN tbl_relasi b ON a.id_relasi = b.id_relasi 
			LEFT JOIN tbl_gejala d ON d.id_gejala = a.id_gejala "
    	);
   	 return $query->result();
	}
	
}