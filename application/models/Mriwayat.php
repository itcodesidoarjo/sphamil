<?php
	class Mriwayat extends CI_Model
{
	public function index_user($id){

    	$query=$this->db->query("
    		SELECT * FROM tbl_user WHERE id_user = '".$id."'"
    	);
   	 return $query->result();
	}

	public function index_diagnosa($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_diagnosa a
			LEFT JOIN tbl_gejala b ON b.id_gejala= a.id_gejala
			WHERE id_user = '".$id."'"
    	);
   	 return $query->result();
	}

	public function index_penyakit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_hasil_diagnosa a
			LEFT JOIN tbl_relasi b ON b.id_relasi= a.id_relasi
			LEFT JOIN tbl_penyakit c ON b.id_penyakit= c.id_penyakit
			LEFT JOIN tbl_user d ON d.id_user= a.id_user
			WHERE d.id_user = '".$id."'
			LIMIT 1"
    	);
   	 return $query->row_array();
	}

	public function index_riwayat($id,$type){
		$id = ($type == "2")?' WHERE d.id_user = '.$id :"";
    	$query=$this->db->query("
    		SELECT * FROM tbl_hasil_diagnosa a
			LEFT JOIN tbl_relasi b ON b.id_relasi= a.id_relasi
			LEFT JOIN tbl_penyakit c ON b.id_penyakit= c.id_penyakit
			LEFT JOIN tbl_user d ON d.id_user= a.id_user
			".$id
    	);
   	 return $query->result();
	}
	
}