<?php
	class Mdiagnosa extends CI_Model
{
	public function index_gejala($data){
		$id_gejala = empty($data)?'':'AND id_gejala NOT IN ('.$data.')';

    	$query=$this->db->query("
    		SELECT * FROM tbl_gejala WHERE status_gejala ='1' ".$id_gejala."
			ORDER BY kd_gejala ASC LIMIT 1"
    	);
   	 return $query->result();
	}
	
	public function index_diagnosa($id){
    	$query=$this->db->query("
			SELECT GROUP_CONCAT(id_gejala) AS gejala FROM tbl_diagnosa 
			WHERE id_user = '".$id."'
			GROUP BY id_user"
    	);
   	 	return $query->row_array();
	}

	public function index_jawaban($id){
    	$query=$this->db->query("
			SELECT GROUP_CONCAT(a.id_gejala) AS gejala, GROUP_CONCAT(a.jawab_diagnosa) AS jawab  FROM (
				SELECT a.id_gejala, jawab_diagnosa
				FROM tbl_diagnosa a
				LEFT JOIN tbl_gejala b ON b.id_gejala = a.id_gejala
				WHERE id_user = '".$id."'
				ORDER BY kd_gejala
			) a"
    	);
   	 	return $query->row_array();
	}

	public function index_relasi(){

    	$query=$this->db->query("
    		SELECT GROUP_CONCAT(a.id_gejala) AS gejala, GROUP_CONCAT(a.pilih_relasi) AS pilih, a.id_relasi
			FROM tbl_relasi_detail a
			LEFT JOIN tbl_relasi b ON b.id_relasi = a.id_relasi
			LEFT JOIN tbl_gejala c ON c.id_gejala = a.id_gejala GROUP BY a.id_relasi
			ORDER BY kd_gejala ASC "
    	);
   	 	return $query->result_array();
	}

	public function simpan($data)
	{
	   $this->db->insert('tbl_diagnosa', $data);
	   $id = $this->db->insert_id();
	}

	public function simpan_hasil($data)
	{
	   $this->db->insert('tbl_hasil_diagnosa', $data);
	   $id = $this->db->insert_id();
   	 	return $id;
	}

	public function simpan_hasil2($data)
	{	
		foreach ($data as $key) {
			$query=$this->db->query("
	    		SELECT * FROM tbl_hasil_diagnosa where TRUE AND id_relasi = '".$key['id_relasi']."' AND id_user = '".$key['id_user']."' "
	    	);
			if (empty($query->result())) {
				$this->db->insert('tbl_hasil_diagnosa', $key);
	   			$id = $this->db->insert_id();
			}
		}
   	 	return $this->db->insert_id();
	}

	public function data_hasil($id){
    	$query=$this->db->query("
			SELECT * FROM tbl_hasil_diagnosa 
			WHERE id_user = '".$id."' "
    	);
   	 	return $query->row_array();
	}

}