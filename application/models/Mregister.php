<?php
	class Mregister extends CI_Model
	{

    public function register($enc_password){
			// User data array
			date_default_timezone_set('Asia/Jakarta');

			$dat_user = array(
				'username'		=> $this->input->post('username'),
                'email_user' 	=> $this->input->post('email_user'),
                'password' 		=> $enc_password,
                'alamat_user' 	=> $this->input->post('alamat_user'),
                'telepon_user' 	=> $this->input->post('telepon_user'),
                'tglreg_user'	=> date("Y-m-d h:i:sa"),
                'type_user'		=> "2",
                'nama_user'		=> $this->input->post('username'),

			);
			/*-------Mengambil id users dan mengirimkan ke model-----*/
			$id_akun = $this->tambah_akun($dat_user);
			/*-----------Menangkap Data Dari Form------------------*/
          	/*sukses*/
		  	$this->session->set_flashdata('pesan', 'Registrasi berhasil. Silahkan Login disini!.');
		  	redirect('welcome/#clockdiv'); //mengembalikan halaman setelah berhasil menginputkan data
		}
		
		public function upedit_ver($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_user',$data);
		} 

		public function tambah_akun($data)
	   	{
	       $this->db->insert('tbl_user', $data);
	       $id = $this->db->insert_id();
	       return (isset($id)) ? $id : FALSE;
	   	}


		// register ===================================================chek data=======================================
		public function check_username_exists($username){
			$query = $this->db->get_where('tbl_user', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

	
		public function check_email_exists($email_user){
			$query = $this->db->get_where('tbl_user', array('email_user' => $email_user));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}	
}