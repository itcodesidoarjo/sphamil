<?php
	class Mgejala extends CI_Model
{
	public function index_gejala(){
    	$query=$this->db->query("
    		SELECT * FROM tbl_gejala where status_gejala ='1'"
    	);
   	 return $query->result();
	}

	public function cekkode($kode){
    	$query=$this->db->query("
    	SELECT * FROM tbl_gejala where kd_gejala = '".$kode."'"
    	);
   	 return $query->result();
	}

	public function simpan($data)
	{
	   $this->db->insert('tbl_gejala', $data);
	   $id = $this->db->insert_id();
	}

	public function hapus($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_gejala',$data);
	}

	public function edit($id){
    	$query=$this->db->query("
    		SELECT * FROM tbl_gejala where true and status_gejala ='1' and id_gejala = ".$id
    	);
   	 return $query->result();
	}

	public function cekkodeedit($kode,$id){
    	$query=$this->db->query("
    	SELECT * FROM tbl_gejala where true and kd_gejala = '".$kode."' and id_gejala <> '".$id."'"
    	);
   	 return $query->result();
	}

	public function simpanedit($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_gejala',$data);
	}
    	
}